INSERT INTO public.addressimpl (zipcode, country, city, street, id, state) VALUES ('16340', 'Indonesia', 'Bogor Regency', 'Jl. Prumpung Raya', 1, 'West Java');
INSERT INTO public.addressimpl (zipcode, country, city, street, id, state) VALUES ('16424', 'Indonesia', 'Depok', 'Jl. Raya Nugroho Notosutanto', 2, 'West Java');

INSERT INTO public.creditcardimpl (cardholdername, id, cardsecuritynumber, cardtypeid, cardexpiration, cardnumber) VALUES ('IKHLAS AL-AFFAN', 1, '737', '3', '03-2024', '1500392086912849');

INSERT INTO public.orderimpl (date, idorder, totalorder, id, idcreditcard, idaddress) VALUES ('17-08-2020', '1', 3, 1, 1, 1);
INSERT INTO public.orderimpl (date, idorder, totalorder, id, idcreditcard, idaddress) VALUES ('18-08-2020', '3', 3, 2, 1, 1);
INSERT INTO public.orderimpl (date, idorder, totalorder, id, idcreditcard, idaddress) VALUES ('18-08-2020', '4', 5, 3, 1, 2);

SELECT pg_catalog.setval('public.addressimpl_id_seq', 2, true);
SELECT pg_catalog.setval('public.creditcardimpl_id_seq', 1, true);
SELECT pg_catalog.setval('public.orderimpl_id_seq', 3, true);
